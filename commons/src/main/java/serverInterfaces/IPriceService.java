package serverInterfaces;

import entity.Car;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IPriceService extends Remote {
    double computeSellingPrice(Car car) throws RemoteException;
}
