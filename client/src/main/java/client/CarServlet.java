package client;

import entity.Car;
import serverInterfaces.IPriceService;
import serverInterfaces.ITaxService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class CarServlet extends HttpServlet {

    private ITaxService taxServerInterface;
    private IPriceService priceServerInterface;
    @Override
    public void init() throws ServletException {
        try {
            Registry registry = LocateRegistry.getRegistry("localhost",1099);
            taxServerInterface = (ITaxService) registry.lookup("serverInterfaces.ITaxService");
            priceServerInterface=(IPriceService) registry.lookup("serverInterfaces.IPriceService");
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String taxMessage="",priceMessage="";
        if(request.getSession().getAttribute("taxPrice")!=null){
            taxMessage=request.getSession().getAttribute("taxPrice").toString();
        }
        if(request.getSession().getAttribute("sellingPrice")!=null){
            priceMessage=request.getSession().getAttribute("sellingPrice").toString();
        }
        String page = "<!doctype html public \"-//w3c//dtd html 4.0 " + "transitional//en\">\n" +
                "<html>\n" +
                "   <body>\n" +
                taxMessage+
                "         <br>" +
                priceMessage+
                "      <form action = \"/car\" method = \"POST\">\n" +
                "         Year: <input type = \"text\" name = \"year\">\n" +
                "         <br />\n" +
                "         Engine Size: <input type = \"text\" name = \"engineSize\" />\n" +
                "         <br>" +
                "         Purchasing Price: <input type = \"text\" name = \"purchasingPrice\" />\n" +
                "         <br>" +
                "         <input type=\"submit\" name=\"action\" value=\"ComputeTax\">\n" +
                "         <input type=\"submit\" name=\"action\" value=\"ComputeSellingPrice\">\n"+
                "      </form>\n" +
                "   </body>\n" +
                "</html>";
        response.setContentType("text/html");

        PrintWriter out = response.getWriter();
        out.println(page);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String action=request.getParameter("action");
            switch (action) {
                case "ComputeTax":
                    computeTax(request, response);
                    break;
                case "ComputeSellingPrice":
                    computeSellingPrice(request, response);
                    break;
                default:
                    break;
            }
    }

    private void computeSellingPrice(HttpServletRequest request, HttpServletResponse response) throws IOException {
        int year = Integer.parseInt(request.getParameter("year"));
        int engineSize = Integer.parseInt(request.getParameter("engineSize"));
        double purchasingPrice = Double.parseDouble(request.getParameter("purchasingPrice"));
        Car car = new Car(year, engineSize, purchasingPrice);
        double sellingPrice = priceServerInterface.computeSellingPrice(car);
        request.getSession().setAttribute("sellingPrice","Selling price is  "+sellingPrice);
        response.sendRedirect("car");

    }

    private void computeTax(HttpServletRequest request, HttpServletResponse response) throws IOException {
        int year = Integer.parseInt(request.getParameter("year"));
        int engineSize = Integer.parseInt(request.getParameter("engineSize"));
        double purchasingPrice = Double.parseDouble(request.getParameter("purchasingPrice"));
        Car car = new Car(year, engineSize, purchasingPrice);
        double taxPrice = taxServerInterface.computeTax(car);
        request.getSession().setAttribute("taxPrice","Tax price is "+taxPrice);
        response.sendRedirect("car");
    }
}
