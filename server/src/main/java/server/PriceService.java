package server;

import entity.Car;
import serverInterfaces.IPriceService;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class PriceService implements IPriceService {
    public double computeSellingPrice(Car car)  {
        double sellingPrice;
        if(2018-car.getYear()<7){
            sellingPrice=car.getPurchasingPrice()-(car.getPurchasingPrice()/7)*(2018-car.getYear());
        }
        else{
            sellingPrice=0;
        }
        return sellingPrice;
    }

    public void createStubAndBind() throws RemoteException {

        IPriceService stub = (IPriceService) UnicastRemoteObject.exportObject((IPriceService) this, 0);
        Registry registry = LocateRegistry.getRegistry();
        registry.rebind("serverInterfaces.IPriceService", stub);
    }
}
