package server;

import entity.Car;
import serverInterfaces.ITaxService;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class TaxService implements ITaxService {

    public double computeTax(Car car) {
        int sum = 8;
        if(car.getEngineSize() > 1601) sum = 18;
        if(car.getEngineSize()> 2001) sum = 72;
        if(car.getEngineSize() > 2601) sum = 144;
        if(car.getEngineSize() > 3001) sum = 290;
        return car.getEngineSize()/200.0*sum;
    }

    public void createStubAndBind() throws RemoteException {

        ITaxService stub = (ITaxService) UnicastRemoteObject.exportObject((ITaxService) this, 0);
        Registry registry = LocateRegistry.getRegistry();
        registry.rebind("serverInterfaces.ITaxService", stub);
    }
}
