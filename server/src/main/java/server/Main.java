package server;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Main {

    public static void main(String[] args) throws RemoteException {
        Registry registryCreated = LocateRegistry.createRegistry(1099);
        TaxService taxServer = new TaxService();
        PriceService priceServer=new PriceService();
        taxServer.createStubAndBind();
        priceServer.createStubAndBind();
    }
}
